# cci-sample

## H2 Git
* git clone from https://gitlab.com/shared-knowledge/cci-sample
* view supporting docker image for CI at https://gitlab.com/shared-knowledge/cci-sample-mysql-ci

## Dependencies
To run the solution locally, you'd need to have 
* IIS with MySQL running locally OR
* Docker and docker-compose installed.

## Runnning Locally
* Using IISExpress : Set the CCI.Sample.Api as start project and run
* Using docker-compose :  Set the docker-compose file as startup project and run. (Docker and docker-compose need to be installed for this to happen)

## Workflow 
* Local Dev : docker-compose or IISExpress
* Source Control : Gitlab for source control
* CI/CD : Codefresh ===> Running unit tests, building docker images, pushing images to image repo and deploying images to kubernetes. The deployment steps are all described in the ./CCI.Sample/build.fsx file and orchestrated using the ./codefresh yaml file. For more on codefresh (https://codefresh.io/)
* Running Docker Containers : Google Kubernetes engine (GKE)
* Database : Google Cloud SQL (CloudSQL)

## Project Solution
### CCI.Sample.Api : Client. exposese the following endpoints
1. api/health :  basic health check. 200 means we can reach our Api endpoint
2. db/health: Db health check does a test insert into the DB. 200 means api and db are up and running 
3. comms/sendTestComms : Implements a basic comms sender. Actual email sending is done using sendbird. If you use a valid email, 200 means an email has been sent out to you. Most likely in your spam folder
4. auth/register : 200 means an authentication record has be saved in the db with id and password provided. Please use a guid for you UserId
5. auth/login : on 200, an AuthenticationToken is returned. You obviously have to use the credentialas you just registered with
6. auth/ping : This validates the AuthenticationToken recieved from auth/login. 200 indicates AuthenticationToken is a valid JWT token

Please look in the documentations folder in the root directory to see images showing successful postman request and expected responses. These have been tested localy and in the running d

#### Local Address
* IISExpress : http://localhost:61164
* Docker : http://localhost:5080

* Swagger page IISExpress : http://localhost:61164/index.html
* Swagger page Docker : http://localhost:5080/index.html

#### Cloud Address
* GKE : http://35.202.87.86
* GKE Swagger page : http://35.202.87.86/index.html

### CCI.Sample.Core : The meaty stuff
This is the flesh of the microservice. I've arranged things in folders, it should be easy to follow. has the Business Logic and DataAccess setup

### CCI.Sample.DbMigrations 
This handle our database management. This is a console application that leverages on the DbUp .NET library. When teh project is run, it :
* Ensures that the Database is up in the environmen pointed to
* Ensures that all tables and table updates have be excuted against said Database. If a script has been executed on previous deploy, it is skipped in the current deploy
For more info on DbUp :  https://dbup.readthedocs.io/en/latest/

### CCI.Sample.UnitTests
This project contains a few unit tests for the solution

### View CI/CD Pipeline on Codefresh
* For a quick view of one of the  pipelines : https://g.codefresh.io/public/accounts/danieleferemo/builds/5cfda920898326d49ab310db
* To view a pipeline, follow the steps : 
1. On Gitlab : On the left had bar, go to CI/CD => Pipelines
2. In Pipelines, under Status Click on any passed status, (would be green)
3. Under Pipeline => External => Click on Codefresh-cci-sample. This should redirect you to a codefresh pipeline.