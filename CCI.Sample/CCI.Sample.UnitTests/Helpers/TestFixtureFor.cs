﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.UnitTests.Helpers
{
    public abstract class TestFixtureFor<TSut> where TSut : class
    {
        private TSut _sut;
        protected TSut Sut => _sut ?? (_sut = ConstructSystemUnderTest());

        private readonly Dictionary<Type, object> _mocks = new Dictionary<Type, object>();

        protected Mock<TDependency> GetMock<TDependency>() where TDependency : class
        {
            var mock = new Mock<TDependency>();
            if (_mocks.ContainsKey(typeof(TDependency)))
                return _mocks[typeof(TDependency)] as Mock<TDependency>;

            _mocks.Add(typeof(TDependency), mock);
            return mock;
        }

        protected abstract TSut ConstructSystemUnderTest();
    }
}
