﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.UnitTests.Helpers
{
    public static class Cloner
    {
        public static T Clone<T>(this T objectToClone)
        {
            var type = typeof(T);
            var instance = (T)Activator.CreateInstance(type);

            foreach (var property in type.GetProperties())
            {
                var originalValue = property.GetValue(objectToClone);
                property.SetValue(instance, originalValue);
            }

            return instance;
        }
    }
}
