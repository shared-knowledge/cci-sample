﻿using CCI.Sample.Core;
using CCI.Sample.Core.Commands;
using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Entities;
using CCI.Sample.Core.Exceptions;
using CCI.Sample.Core.Handlers.Security;
using CCI.Sample.Core.Services;
using CCI.Sample.UnitTests.Helpers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace CCI.Sample.UnitTests
{
    public class AuthenticationServiceTests : TestFixtureFor<AuthenticationService>
    {
        private PasswordHandler _passwordHandler;
        private AuthenticationTokenHandler _authTokenHandler;

        private AuthenticationEntity _authEntity;
        private Guid _userId = GuidComb.Generate();
        private readonly string _password = "Passw0rd";
        private AppSettings _appSettings = new AppSettings() { };

        public AuthenticationServiceTests()
        {
            _passwordHandler = new PasswordHandler();
            var saltBytes = _passwordHandler.GenerateSalt();
            var hashBytes = _passwordHandler.CreatePasswordHash(_password, saltBytes);

            var jwtSettings = new JwtSettings()
            {
                JwtKey = "1234567890123456789",
                TokenExpiryMinutes = 1,
                ValidAudience = "aud",
                ValidIssuer = "iss"
            };

            _appSettings.AuthenticationSettings = new AuthenticationSettings() { JwtSettings = jwtSettings };
            _authTokenHandler = new AuthenticationTokenHandler(_appSettings);

            _authEntity = new AuthenticationEntity() { UserId = _userId, Salt = saltBytes, Hash = hashBytes };

            GetMock<IUnitOfWork>().Setup(x => x.Repository<AuthenticationEntity>().FirstOrDefaultAsync(It.IsAny<Expression<Func<AuthenticationEntity, bool>>>()))
                                    .ReturnsAsync(_authEntity);
        }

        protected override AuthenticationService ConstructSystemUnderTest()
        {
            return new AuthenticationService(
                GetMock<IUnitOfWork>().Object,
                GetMock<ILogger<AuthenticationService>>().Object,
                _passwordHandler,
                _authTokenHandler);
        }

        [Fact]
        public async Task Successful_Login_Should_Generate_Valid_Jwt_Token()
        {
            var loginCommand = new LoginCommand() { UserId = _userId, Password = _password };

            string token = await Sut.LoginAsync(loginCommand);
            ClaimsPrincipal claimsPrincipal = _authTokenHandler.ValidateToken(token);

            Assert.NotNull(claimsPrincipal);
            Assert.True(claimsPrincipal.Identity.IsAuthenticated);
            Assert.NotNull(claimsPrincipal.Claims.First(x => x.Type == "UserId"));
        }

        [Fact]
        public void Throws_Exception_On_Empty_Password()
        {
            var loginCommand = new LoginCommand() { UserId = _userId };

            Action act = () => Sut.LoginAsync(loginCommand).GetAwaiter().GetResult();

            Assert.Throws<BadRequestException>(act);
        }

        [Fact]
        public void Throws_Exception_On_Invalid_Password()
        {
            var loginCommand = new LoginCommand() { UserId = _userId, Password = "Invalid Password" };

            Action act = () => Sut.LoginAsync(loginCommand).GetAwaiter().GetResult();

            Assert.Throws<UnauthorizedAccessException>(act);
        }
    }
}
