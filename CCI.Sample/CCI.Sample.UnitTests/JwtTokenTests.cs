﻿using CCI.Sample.Core;
using CCI.Sample.Core.Handlers.Security;
using CCI.Sample.UnitTests.Helpers;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Xunit;

namespace CCI.Sample.UnitTests
{
    public class JwtTokenTests
    {
        private AuthenticationTokenHandler _tokenHandler;
        private const string _name = "bob";
        private const string _claimsType = "name";
        private AppSettings _appSettings = new AppSettings() { };
        private readonly IEnumerable<Claim> _claims = new Claim[] { new Claim(_claimsType, _name) };
        private static readonly JwtSettings _jwtSettings = new JwtSettings()
        {
            JwtKey = "1234567890123456789",
            TokenExpiryMinutes = 1,
            ValidAudience = "aud",
            ValidIssuer = "iss",
        };

        public JwtTokenTests()
        {
            _appSettings.AuthenticationSettings = new AuthenticationSettings() { JwtSettings = _jwtSettings };
            _tokenHandler = new AuthenticationTokenHandler(_appSettings);
        }

        [Fact]
        public void Successfully_Create_And_Validate_JWT_Token()
        {
            string token = _tokenHandler.CreateAuthToken(_claims);
            var claimsPrinciple = _tokenHandler.ValidateToken(token);
            var actualName = claimsPrinciple.Claims.SingleOrDefault(x => x.Type == _claimsType)?.Value;

            Assert.Equal(_name, actualName);
        }

        [Fact]
        public void ThrowsException_When_WrongJwtToken()
        {
            string token = "Invalid Token";
            Action act = () => _tokenHandler.ValidateToken(token);

            Assert.Throws<UnauthorizedAccessException>(act);
        }

        [Fact]
        public void ThrowsException_When_WrongAudiance()
        {
            var jwtSettings = _appSettings.AuthenticationSettings.JwtSettings.Clone();
            jwtSettings.ValidAudience = "wrong audiance";

            var appSettings = new AppSettings() { AuthenticationSettings = new AuthenticationSettings() { JwtSettings = jwtSettings} };
            var tokenHandlerValidator = new AuthenticationTokenHandler(appSettings);

            string token = _tokenHandler.CreateAuthToken(_claims);
            Action act = () => tokenHandlerValidator.ValidateToken(token);

            Assert.Throws<UnauthorizedAccessException>(act);
        }

        [Fact]
        public void ThrowsException_When_WrongIssuer()
        {
            var jwtSettings = _appSettings.AuthenticationSettings.JwtSettings.Clone();
            jwtSettings.ValidIssuer = "wrong Issuer";

            var appSettings = new AppSettings() { AuthenticationSettings = new AuthenticationSettings() { JwtSettings = jwtSettings } };
            AuthenticationTokenHandler tokenHandlerValidator = new AuthenticationTokenHandler(appSettings);

            string token = _tokenHandler.CreateAuthToken(_claims);
            Action act = () => tokenHandlerValidator.ValidateToken(token);

            Assert.Throws<UnauthorizedAccessException>(act);
        }

        [Fact]
        public void ThrowsException_When_WrongJwtKey()
        {
            var jwtSettings = _appSettings.AuthenticationSettings.JwtSettings.Clone();
            jwtSettings.JwtKey = "wrong JwtKey";

            var appSettings = new AppSettings() { AuthenticationSettings = new AuthenticationSettings() { JwtSettings = jwtSettings } };
            AuthenticationTokenHandler tokenHandlerValidator = new AuthenticationTokenHandler(appSettings);

            string token = _tokenHandler.CreateAuthToken(_claims);
            Action act = () => tokenHandlerValidator.ValidateToken(token);

            Assert.Throws<UnauthorizedAccessException>(act);
        }
    }
}
