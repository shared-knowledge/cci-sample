﻿using CCI.Sample.Core.Entities;

namespace CCI.Sample.Core.Models
{
    public interface ICommsModel
    {
        int UserId { get; set; }
        string Email { get; set; }
        CommsType CommsType { get; set; }
    }
}