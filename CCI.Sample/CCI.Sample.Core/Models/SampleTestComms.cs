﻿using CCI.Sample.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Models
{
    public class SampleTestComms : ICommsModel
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public CommsType CommsType { get; set; } = CommsType.SampleTestEmail;
    }
}
