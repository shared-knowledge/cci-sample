﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Models
{
    public class Email
    {
        public string ToEmailAddress { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string FullName { get; set; }
    }
}
