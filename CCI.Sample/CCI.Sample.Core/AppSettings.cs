﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core
{
    public class AppSettings
    {
        public CommsSettings CommsSettings { get; set; }
        public Dictionary<string, string> ApiEndpoints { get; set; }
        public AuthenticationSettings AuthenticationSettings { get; set; }

    }
    public class CommsSettings
    {
        public string CM_SendGridApiKey_WIP { get; set; }
        public string SendGridEmailName { get; set; }
        public string SendGridNoReplyEmail { get; set; }
        public string EmailValidationRegex { get; set; }
        public int UserIdUpperLimit { get; set; }
    }
    public class AuthenticationSettings
    {
        public JwtSettings JwtSettings { get; set; }
    }

    public class JwtSettings
    {
        public string JwtKey { get; set; }
        public string ValidIssuer { get; set; }
        public string ValidAudience { get; set; }
        public int TokenExpiryMinutes { get; set; }
    }
}
