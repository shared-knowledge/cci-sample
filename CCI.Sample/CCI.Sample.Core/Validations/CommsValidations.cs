﻿using CCI.Sample.Core.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CCI.Sample.Core.Validations
{
    public class CommsValidations
    {
        public static void ValidateEmail(Type type, string Email, string emailValidationRegex, ILogger logger)
        {
            if (string.IsNullOrEmpty(Email))
            {
                logger.LogError($"{type.Name} Validation failed. Email not present in model.");
                throw new BadRequestException("Email not present in model.");
            }

            if (!Regex.IsMatch(Email, emailValidationRegex, RegexOptions.IgnoreCase))
            {
                logger.LogError($"{type.Name} Validation failed: Email not in correct format");
                throw new BadRequestException("Email not in correct format");
            }
        }
    }
}
