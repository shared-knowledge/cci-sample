﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.AspNetExtensions
{
    public static class SwaggerDocumentationServicesExtensions
    {
        public static IServiceCollection AddSwaggerDocumentationService(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Sample API for CCI GrowthCon", Version = "v1" });
            });

            return services;
        }
    }
}
