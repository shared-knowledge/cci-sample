﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.AspNetExtensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder WithConfiguration(this IConfigurationBuilder builder, IHostingEnvironment env)
        {
            return builder
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }
    }
}
