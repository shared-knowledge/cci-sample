﻿using CCI.Sample.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Providers
{
    public interface IEmailCommsProvider
    {
        Task SendEmailAsync(Email email);
    }
}
