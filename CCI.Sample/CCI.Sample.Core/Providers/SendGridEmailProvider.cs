﻿using CCI.Sample.Core.Exceptions;
using CCI.Sample.Core.Models;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Providers
{
    public class SendGridEmailProvider : IEmailCommsProvider
    {
        private readonly CommsSettings _commsSettings;

        public SendGridEmailProvider(IOptions<AppSettings> appSettings)
        {
            _commsSettings = appSettings.Value.CommsSettings;
        }

        public async Task SendEmailAsync(Email email)
        {
            await SendMail(email);
        }

        private async Task SendMail(Email email)
        {
            var sendGridMessage = MailHelper.CreateSingleEmail(new EmailAddress(_commsSettings.SendGridNoReplyEmail, _commsSettings.SendGridEmailName),
                                                                new EmailAddress(email.ToEmailAddress, email.FullName),
                                                                email.Subject,
                                                                email.Content,
                                                                email.Content);

            var sendGridClient = new SendGridClient(_commsSettings.CM_SendGridApiKey_WIP);
            var sendGridResponse = await sendGridClient.SendEmailAsync(sendGridMessage);

            if (sendGridResponse.StatusCode != System.Net.HttpStatusCode.Accepted)
                throw new EmailNotSentException($"Email sending with SendGrid failed for email=[{email.ToEmailAddress}] with status code=[{sendGridResponse.StatusCode}]");
        }
    }
}
