﻿using CCI.Sample.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.DataAccess
{
    public class MyDatabaseContext : DbContext, IDatabaseContext
    {
        public MyDatabaseContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TestEntity>(x =>
            {
                x.ToTable("test");

                x.HasKey(e => e.Id);

                x.Property(e => e.Content).IsRequired().HasColumnType("VARCHAR(255)");

                x.Property(t => t.DateCreated).HasComputedColumnSql("datetime");
            });

            modelBuilder.Entity<CommsEntity>(x =>
            {
                x.ToTable("comms");

                x.HasKey(e => e.Id);

                x.Property(e => e.Email).IsRequired().HasColumnType("VARCHAR(255)");

                x.Property(e => e.CommsType).IsRequired().HasColumnType("VARCHAR(255)");
                x.Property(e => e.CommsContent).IsRequired().HasColumnType("VARCHAR(255)");

                x.Property(t => t.CreatedOn).IsRequired().HasComputedColumnSql("datetime");
            });

            modelBuilder.Entity<AuthenticationEntity>(x =>
            {
                x.ToTable("authentication");

                x.HasKey(e => e.UserId);

                x.Property(e => e.Salt).IsRequired().HasColumnType("CHAR(128)");
                x.Property(e => e.Hash).IsRequired().HasColumnType("CHAR(128)");

                x.Property(t => t.CreatedOn).IsRequired().HasComputedColumnSql("datetime");
            });
        }
    }
}
