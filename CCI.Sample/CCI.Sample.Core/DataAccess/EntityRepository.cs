﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CCI.Sample.Core.DataAccess
{
    public class EntityRepository<T> : IEntityRepository<T> where T : class
    {
        private readonly IDatabaseContext _databaseContext;

        public EntityRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Add(T entity)
        {
            _databaseContext.Set<T>().AddAsync(entity);
        }

        public IQueryable<T> GetAllAsync(Expression<Func<T, bool>> expression)
        {
            return _databaseContext.Set<T>().Where(expression);
        }

        public IQueryable<T> GetAllPagedAsync(int pageindex, int pagesize, Expression<Func<T, bool>> expression)
        {
            return _databaseContext.Set<T>().Where(expression).Skip(pageindex * pagesize).Take(pagesize);
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            return await _databaseContext.Set<T>().FirstOrDefaultAsync(expression);
        }

        public void Update(T entity)
        {
            _databaseContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            _databaseContext.Set<T>().Remove(entity);
        }
    }
}