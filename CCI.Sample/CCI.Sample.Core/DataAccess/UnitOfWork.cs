﻿using System.Threading;
using System.Threading.Tasks;

namespace CCI.Sample.Core.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseContext _databaseContext;

        public UnitOfWork(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public async Task CommitAsync()
        {
            await _databaseContext.SaveChangesAsync(default(CancellationToken));
            return;
        }

        public IEntityRepository<T> Repository<T>() where T : class
        {
            return new EntityRepository<T>(_databaseContext);
        }

        public void Dispose()
        {
            _databaseContext.Dispose();
        }
    }
}
