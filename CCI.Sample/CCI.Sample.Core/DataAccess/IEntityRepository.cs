﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CCI.Sample.Core.DataAccess
{
    public interface IEntityRepository<T> where T : class
    {
        void Add(T entity);
        IQueryable<T> GetAllAsync(Expression<Func<T, bool>> expression);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression);
        void Update(T entity);
        void Delete(T entity);
    }
}
