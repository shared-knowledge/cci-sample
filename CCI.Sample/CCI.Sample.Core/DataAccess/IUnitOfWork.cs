﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        IEntityRepository<T> Repository<T>() where T : class;
        Task CommitAsync();
    }
}