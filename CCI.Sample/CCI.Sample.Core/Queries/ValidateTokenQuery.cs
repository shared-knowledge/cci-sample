﻿using CCI.Sample.Core.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Queries
{
    public class ValidateTokenQuery
    {
        public string AuthToken { get; set; }

        public void Validate(ILogger logger)
        {
            if (string.IsNullOrEmpty(AuthToken))
            {
                logger.LogError("ValidateTokenQuery Validation failed: Require AuthToken in model");
                throw new BadRequestException("Require Auth Token");
            }
        }
    }
}
