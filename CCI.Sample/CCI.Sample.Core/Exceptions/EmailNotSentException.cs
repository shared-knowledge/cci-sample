﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Exceptions
{
    public class EmailNotSentException : Exception
    {
        public EmailNotSentException(string message) : base(message)
        {
        }
    }
}
