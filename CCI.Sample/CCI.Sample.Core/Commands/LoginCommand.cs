﻿using CCI.Sample.Core.Exceptions;
using Microsoft.Extensions.Logging;
using System;

namespace CCI.Sample.Core.Commands
{
    public class LoginCommand
    {
        public Guid UserId { get; set; }
        public string Password { get; set; }

        public void Validate(ILogger logger)
        {
            if (UserId == null || UserId == Guid.Empty)
            {
                logger.LogError("LoginCommand Validation failed: Require UserId in model");
                throw new BadRequestException("Require UserId");
            }

            if (string.IsNullOrEmpty(Password))
            {
                logger.LogError("LoginCommand Validation failed: Require Password in model");
                throw new BadRequestException("Require Password");
            }
        }
    }
}
