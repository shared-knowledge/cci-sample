﻿using CCI.Sample.Core.Entities;
using CCI.Sample.Core.Exceptions;
using CCI.Sample.Core.Models;
using CCI.Sample.Core.Validations;
using Microsoft.Extensions.Logging;

namespace CCI.Sample.Core.Commands
{
    public class SendTestCommsCommand
    {
        public int UserId { get; set; }
        public string Email { get; set; }

        public void Validate(string emailValidationRegex, int UserIdLimit, ILogger logger)
        {
            if (UserId <= 0 || UserId > UserIdLimit)
            {
                logger.LogError("SendTestCommsCommand Validation failed: Require UserId to be between 1 and 10");
                throw new BadRequestException("UserId to be between 1 and 10");
            }
            CommsValidations.ValidateEmail(typeof(SendTestCommsCommand), Email, emailValidationRegex, logger);
        }
    }
}
