﻿using CCI.Sample.Core.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Commands
{
    public class CreateAuthenticationCommand
    {
        public Guid UserId { get; set; }
        public string Password { get; set; }

        public void Validate(ILogger logger)
        {
            if (string.IsNullOrEmpty(Password))
            {
                logger.LogError("CreateAuthenticationCommand Validation failed: Requires Password in model");
                throw new BadRequestException("Require Password");
            }

            if (UserId == null || UserId == Guid.Empty)
            {
                logger.LogError("CreateAuthenticationCommand Validation failed: Requires UserId in model");
                throw new BadRequestException("Require UserId");
            }
        }
    }
}
