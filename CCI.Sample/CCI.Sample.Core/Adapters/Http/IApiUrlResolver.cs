﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Adapters.Http
{
    public interface IApiUrlResolver
    {
        Task<string> ResolveAsync(Type requestType);
    }
}