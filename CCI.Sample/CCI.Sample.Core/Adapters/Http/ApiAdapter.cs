﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Net.Http.Headers;

namespace CCI.Sample.Core.Adapters.Http
{
    public abstract class ApiAdapter
    {
        protected Lazy<Task<string>> ResolveUrl { get; set; }
    }

    public abstract class ApiAdapter<TRequest, TResponse> : ApiAdapter, IApiAdapter<TRequest, TResponse>
    {
        protected IHttpClientFactory HttpClientFactory { get; }

        public ApiAdapter(IHttpClientFactory httpClientFactory)
        {
            HttpClientFactory = httpClientFactory;
        }

        protected async Task<HttpResponseMessage> GetHttpResponseMessageAsync(TRequest query)
        {
            var url = await ResolveUrl.Value;
            var client = HttpClientFactory.CreateClient();
            var uriBuilder = new UriBuilder(url);

            var queryParams = HttpUtility.ParseQueryString(uriBuilder.Query);

            var builtUrl = new UrlBuilder().WithBaseUrl(uriBuilder.ToString())
                                            .WithQueryParam(MapQueryParameters(query, queryParams))
                                            .Build();

            var request = new HttpRequestMessage(HttpMethod.Get, builtUrl);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return await client.SendAsync(request).ConfigureAwait(false);
        }

        protected string MapQueryParameters(object instance, NameValueCollection queryParameters)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var prop in instance.GetType().GetProperties())
            {
                var value = prop.GetValue(instance, null);
                if (value != null)
                    sb.Append(value+"/");
            }

            return sb.ToString();
        }
    }
}
