﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace CCI.Sample.Core.Adapters.Http
{
    public class UrlBuilder
    {
        private string _baseUrl;

        private string _queryParams;

        public UrlBuilder WithBaseUrl(string baseUrl)
        {
            _baseUrl = baseUrl;
            return this;
        }

        public UrlBuilder WithQueryParam(string value)
        {
            _queryParams += value;
            return this;
        }

        public string Build()
        {
            var baseAddress = CombineUrl(_baseUrl, _queryParams);

            var uriBuilder = new UriBuilder(baseAddress);

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);

            uriBuilder.Query = query.ToString();

            return uriBuilder.ToString();
        }

        private static string CombineUrl(string url1, string url2)
        {
            url1 = url1?.TrimEnd('/');
            url2 = url2?.TrimStart('/');

            return $"{url1}/{url2}";
        }
    }
}
