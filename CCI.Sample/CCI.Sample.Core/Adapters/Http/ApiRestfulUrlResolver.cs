﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CCI.Sample.Core.Adapters.Http
{
    public class ApiRestfulUrlResolver : ApiUrlResolver
    {
        public ApiRestfulUrlResolver(IOptions<AppSettings> appSettings) : base(appSettings)
        {
        }

        public override Task<string> ResolveAsync(Type requestType)
        {
            var apiEndpoints = AppSettings.ApiEndpoints;

            var queryAttribute = (ApiCallAttribute)requestType.GetCustomAttributes(typeof(ApiCallAttribute), false).First();

            var configName = queryAttribute.ServiceKey;
            var queryName = queryAttribute.Name;

            if (!apiEndpoints.TryGetValue(configName, out var url) || string.IsNullOrEmpty(url))
                throw new Exception($"No API configuration found for APIEndpoint setting: {configName}");

            var baseAddress = $"{url}/{queryName}";
            var uriBuilder = new UriBuilder(baseAddress);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            uriBuilder.Query = query.ToString();

            return Task.FromResult(uriBuilder.ToString());
        }
    }
}
