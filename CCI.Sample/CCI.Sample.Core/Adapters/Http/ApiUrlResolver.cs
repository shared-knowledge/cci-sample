﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Adapters.Http
{
    public abstract class ApiUrlResolver : IApiUrlResolver
    {
        protected AppSettings AppSettings;

        public ApiUrlResolver(IOptions<AppSettings> appSettings)
        {
            AppSettings = appSettings.Value;
        }
        public abstract Task<string> ResolveAsync(Type requestType);
    }
}
