﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Adapters.Http
{
    public class QueryAdapter<TQuery, TResponse> : ApiAdapter<TQuery, TResponse>, IQueryAdapter<TQuery, TResponse> where TResponse : new()
    {
        public QueryAdapter(IHttpClientFactory httpClientFactory, IApiUrlResolver resolver) : base(httpClientFactory)
        {
            var requestType = typeof(TQuery);
            ResolveUrl = new Lazy<Task<string>>(() => resolver.ResolveAsync(requestType));
        }

        public async Task<TResponse> GetAsync(TQuery query)
        {
            var response = await GetHttpResponseMessageAsync(query);
            response.EnsureSuccessStatusCode();
            var str = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResponse>(str);
        }
    }
}
