﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Adapters.Http
{
    public interface IQueryAdapter<in TQuery, TResponse>
    {
        Task<TResponse> GetAsync(TQuery query);
    }
}
