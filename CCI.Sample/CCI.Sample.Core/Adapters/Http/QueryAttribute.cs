﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Adapters.Http
{
    public class QueryAttribute : ApiCallAttribute
    {
        public QueryAttribute(string serviceKey, string name) : base(serviceKey, name)
        {
        }
    }
}
