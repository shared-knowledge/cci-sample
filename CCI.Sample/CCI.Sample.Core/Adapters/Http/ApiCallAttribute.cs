﻿using System;

namespace CCI.Sample.Core.Adapters.Http
{
    public class ApiCallAttribute : Attribute
    {
        public string ServiceKey { get; }

        public string Name { get; }

        public ApiCallAttribute(string serviceKey, string name)
        {
            ServiceKey = serviceKey;
            Name = name;
        }
    }
}
