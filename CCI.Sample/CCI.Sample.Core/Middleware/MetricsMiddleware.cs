﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Middleware
{
    public class MetricsMiddleware
    {
        private readonly RequestDelegate _requestDelegate;
        readonly ILogger<MetricsMiddleware> _logger;

        public MetricsMiddleware(RequestDelegate requestDelegate, ILogger<MetricsMiddleware> logger)
        {
            _logger = logger;
            _requestDelegate = requestDelegate;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            _logger.LogInformation($"Handling Http Request [{httpContext.Request.Method}] {httpContext.Request.Path}");

            var sw = new Stopwatch();

            sw.Start();
            await _requestDelegate(httpContext);
            sw.Stop();

            _logger.LogInformation($"Http request completed: [{httpContext.Request.Method}] {httpContext.Request.Path}, Duration: {sw.Elapsed.TotalMilliseconds} milliseconds");
        }
    }
}
