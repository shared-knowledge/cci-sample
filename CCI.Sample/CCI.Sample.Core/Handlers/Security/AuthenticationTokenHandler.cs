﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace CCI.Sample.Core.Handlers.Security
{
    public class AuthenticationTokenHandler
    {
        readonly JwtSettings _jwtSettings;

        public AuthenticationTokenHandler(AppSettings appSettings)
        {
            _jwtSettings = appSettings.AuthenticationSettings.JwtSettings;
        }

        public string CreateAuthToken(IEnumerable<Claim> claims)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtKeyBytes = Encoding.UTF8.GetBytes(_jwtSettings.JwtKey);

            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                Issuer = _jwtSettings.ValidIssuer,
                Audience = _jwtSettings.ValidAudience,
                Expires = DateTime.UtcNow.AddMinutes(_jwtSettings.TokenExpiryMinutes),
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(jwtKeyBytes), SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha512Digest)
            };

            var authToken = handler.CreateToken(securityTokenDescriptor);

            return handler.WriteToken(authToken);
        }


        /// <summary>
        /// Example method to validate a token. Used in Unit Test and Auth/Ping Endpoint
        /// </summary>
        /// <param name="token">token.</param>
        public ClaimsPrincipal ValidateToken(string token)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();

                var param = new TokenValidationParameters
                {
                    ClockSkew = TimeSpan.Zero,
                    ValidIssuer = _jwtSettings.ValidIssuer,
                    ValidAudience = _jwtSettings.ValidAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.JwtKey)),
                };
                return handler.ValidateToken(token, param, out SecurityToken validatedToken);
            }
            catch
            {
                throw new UnauthorizedAccessException("Invalid JWT");
            }
        }
    }
}
