﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CCI.Sample.Core.Handlers.Security
{
    public class PasswordHandler
    {
        const int _minSaltSize = 4;
        const int _maxSaltSize = 8;

        public byte[] GenerateSalt()
        {
            Random random = new Random();

            // Generate random number for size of the salt
            int saltSize = random.Next(_minSaltSize, _maxSaltSize);
            byte[] saltBytes = new byte[saltSize];

            // Initialize random number generator
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            // Fill the salt byte array with cryptographiaclly strong byte non zero values
            rng.GetNonZeroBytes(saltBytes);

            return saltBytes;
        }

        public byte[] CreatePasswordHash(string password, byte[] salt)
        {
            if (salt == null || salt.Length < 4)
            {
                throw new Exception("Salt is too weak");
            }

            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] passwordAndSaltBytes = new byte[passwordBytes.Length + salt.Length];

            Array.Copy(passwordBytes, passwordAndSaltBytes, passwordBytes.Length);
            Array.Copy(salt, 0, passwordAndSaltBytes, passwordBytes.Length, salt.Length);

            using (var hash = new SHA512Managed())
            {
                return hash.ComputeHash(passwordAndSaltBytes);
            }
        }

        public bool PasswordIsCorrect(string inputPassword, byte[] storedPasswordHash, byte[] salt)
        {
            if (string.IsNullOrEmpty(inputPassword) || storedPasswordHash == null || storedPasswordHash.Length == 0 || salt == null || salt.Length == 0)
                return false;

            byte[] inputPasswordHash = CreatePasswordHash(inputPassword, salt);
            if (!storedPasswordHash.SequenceEqual(inputPasswordHash))
                return false;

            return true;
        }
    }
}
