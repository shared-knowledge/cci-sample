﻿using CCI.Sample.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Handlers
{
    public interface IHandleComms<TComms> where TComms : ICommsModel
    {
        Task HandleAsync(TComms comms);
    }
}
