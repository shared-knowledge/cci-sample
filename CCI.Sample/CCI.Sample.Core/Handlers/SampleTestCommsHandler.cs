﻿using CCI.Sample.Core.Adapters.Http;
using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Models;
using CCI.Sample.Core.Providers;
using CCI.Sample.Core.Queries;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Handlers
{
    public class SampleTestCommsHandler : CommsHandler<ICommsModel>
    {
        private ILogger<SampleTestCommsHandler> _logger;

        public SampleTestCommsHandler(
            IUnitOfWork unitOfWork,
            IEmailCommsProvider emailCommsProvider,
            ILogger<SampleTestCommsHandler> logger,
            IQueryAdapter<GetUserDetailsQuery, GetUserDetailsQueryResponse> userDetailQueryAdapter) :
            base(emailCommsProvider, unitOfWork, logger, userDetailQueryAdapter)
        {
            _logger = logger;
        }

        protected override Email CompileEmailAsync(ICommsModel comms)
        {
            _logger.LogInformation($"Handling CompileEmailAsync for comms=[{comms.GetType().Name}] for account with name=[{UserDetails.Name}]");

            // Do some email compilation and return in content of email below. 
            // For example, compile a custom razor page with user details already computed in base class and assign html to email content below

            return new Email() {
                ToEmailAddress = comms.Email,
                Subject = "CCI Sample Test Email!",
                FullName = UserDetails.Name,
                Content = "Sample test commms hello world",
            };
        }
    }
}
