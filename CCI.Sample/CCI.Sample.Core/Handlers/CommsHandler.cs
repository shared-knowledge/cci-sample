﻿using CCI.Sample.Core.Adapters.Http;
using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Entities;
using CCI.Sample.Core.Models;
using CCI.Sample.Core.Providers;
using CCI.Sample.Core.Queries;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Handlers
{
    public abstract class CommsHandler<TComms> : IHandleComms<TComms> where TComms : ICommsModel
    {
        protected IUnitOfWork UnitOfWork;
        protected ILogger<CommsHandler<TComms>> Logger;
        protected readonly IEmailCommsProvider EmailCommsProvider;
        private readonly IQueryAdapter<GetUserDetailsQuery, GetUserDetailsQueryResponse> _userDetailQueryAdapter;

        public CommsHandler(IEmailCommsProvider emailCommsProvider,IUnitOfWork unitOfWork, ILogger<CommsHandler<TComms>> logger, IQueryAdapter<GetUserDetailsQuery, GetUserDetailsQueryResponse> userDetailQueryAdapter)
        {
            Logger = logger;
            UnitOfWork = unitOfWork;
            EmailCommsProvider = emailCommsProvider;
            _userDetailQueryAdapter = userDetailQueryAdapter;
        }

        public async Task HandleAsync(TComms comms)
        {
            Logger.LogInformation($"Handling comms of type [{comms.GetType().Name}] for account with name=[{comms.UserId}]");

            await GetUserDetailsAsync(comms.UserId);

            var email = CompileEmailAsync(comms);
            await EmailCommsProvider.SendEmailAsync(email);

            Logger.LogInformation($"Saving comms of type [{comms.GetType().Name}] for account with name=[{comms.UserId}]");
            UnitOfWork.Repository<CommsEntity>().Add(new CommsEntity()
            {
                Id = GuidComb.Generate(),
                Email = comms.Email,
                CommsType = comms.CommsType.ToString(),
                CommsContent = email.Content
            });
            await UnitOfWork.CommitAsync();
        }


        private async Task GetUserDetailsAsync(int userId)
        {
            UserDetails = await _userDetailQueryAdapter.GetAsync(new GetUserDetailsQuery() { UserId = userId });
        }

        protected abstract Email CompileEmailAsync(TComms comms);
        protected GetUserDetailsQueryResponse UserDetails { get; set; }
    }
}
