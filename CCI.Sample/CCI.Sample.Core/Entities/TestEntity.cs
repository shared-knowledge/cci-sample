﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Entities
{
    public class TestEntity
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
