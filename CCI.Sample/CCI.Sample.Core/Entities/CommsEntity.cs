﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Entities
{
    public class CommsEntity
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string CommsType { get; set; }
        public string CommsContent { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public enum CommsType
    {
        SampleTestEmail = 0,
    }
}
