﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CCI.Sample.Core.Entities
{
    public class AuthenticationEntity
    {
        public Guid UserId { get; set; }
        public byte[] Salt { get; set; }
        public byte[] Hash { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
