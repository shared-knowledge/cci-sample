﻿using System.Threading.Tasks;

namespace CCI.Sample.Core.Services
{
    public interface ITestService
    {
        Task TestDbInsertAsync();
    }
}