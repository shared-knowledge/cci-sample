﻿using CCI.Sample.Core.Commands;
using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Entities;
using CCI.Sample.Core.Exceptions;
using CCI.Sample.Core.Handlers.Security;
using CCI.Sample.Core.Queries;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private IUnitOfWork _unitOfWork;
        private readonly PasswordHandler _passwordHandler;
        private readonly ILogger<AuthenticationService> _logger;
        private readonly AuthenticationTokenHandler _authenticationTokenHandler;

        public AuthenticationService(
            IUnitOfWork unitOfWork,
            ILogger<AuthenticationService> logger,
            PasswordHandler passwordHandler,
            AuthenticationTokenHandler authenticationTokenHandler)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _passwordHandler = passwordHandler;
            _authenticationTokenHandler = authenticationTokenHandler;
        }

        public async Task CreateAuthenticationAsync(CreateAuthenticationCommand command)
        {
            _logger.LogInformation($"CreateAuthenticationCommand recieved for User with UserId={command.UserId}");

            command.Validate(_logger);

            var salt = _passwordHandler.GenerateSalt();

            _unitOfWork.Repository<AuthenticationEntity>().Add(new AuthenticationEntity()
            {
                UserId = command.UserId,
                Salt = salt,
                Hash = _passwordHandler.CreatePasswordHash(command.Password, salt)
            });

            await _unitOfWork.CommitAsync();
        }

        public async Task<string> LoginAsync(LoginCommand command)
        {
            _logger.LogInformation($"LoginRequest recieved for User with UserId={command.UserId}");

            command.Validate(_logger);

            var authentication = await _unitOfWork.Repository<AuthenticationEntity>().FirstOrDefaultAsync(x => x.UserId == command.UserId);

            if(authentication == null)
            {
                _logger.LogInformation($"LoginRequest failed. Authentication record not found");
                throw new UnauthorizedAccessException($"LoginRequest failed.");
            }

            if (!_passwordHandler.PasswordIsCorrect(command.Password, authentication.Hash, authentication.Salt))
            {
                _logger.LogInformation($"LoginRequest failed for User with UserId={command.UserId}.");
                throw new UnauthorizedAccessException($"LoginRequest failed.");
            }

            var authClaim = new[] { new Claim("UserId", command.UserId.ToString()) };
            return _authenticationTokenHandler.CreateAuthToken(authClaim);
        }

        public bool ValidateToken(ValidateTokenQuery query)
        {
            _logger.LogInformation($"ValidateTokenQuery recieved");

            query.Validate(_logger);

            ClaimsPrincipal claimsPrincipal = _authenticationTokenHandler.ValidateToken(query.AuthToken);

            if (claimsPrincipal == null)
                throw new BadRequestException("Invalid Auth Token");

            return claimsPrincipal.Identity.IsAuthenticated;
        }
    }
}
