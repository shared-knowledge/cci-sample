﻿using CCI.Sample.Core.Commands;
using CCI.Sample.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Services
{
    public interface ICommsService
    {
        Task HandleSampleTestCommsAsync(SendTestCommsCommand command);
    }
}