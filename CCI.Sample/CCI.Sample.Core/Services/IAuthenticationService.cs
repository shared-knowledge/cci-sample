﻿using CCI.Sample.Core.Commands;
using CCI.Sample.Core.Queries;
using System;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Services
{
    public interface IAuthenticationService
    {
        Task CreateAuthenticationAsync(CreateAuthenticationCommand command);
        Task<string> LoginAsync(LoginCommand command);
        bool ValidateToken(ValidateTokenQuery query);
    }
}