﻿using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Services
{
    public class TestService : ITestService
    {
        private IUnitOfWork _unitOfWork;

        public TestService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task TestDbInsertAsync()
        {
            _unitOfWork.Repository<TestEntity>().Add(new TestEntity() { Content = "Test Service hit at " + DateTime.UtcNow });

            await _unitOfWork.CommitAsync();
            return;
        }
    }
}
