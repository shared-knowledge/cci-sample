﻿using CCI.Sample.Core.Commands;
using CCI.Sample.Core.Handlers;
using CCI.Sample.Core.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Core.Services
{
    public class CommsService : ICommsService
    {
        private readonly ILogger<CommsService> _logger;
        private readonly CommsSettings _commsSettings;

        private readonly IHandleComms<ICommsModel> _sampleTestCommsHandler;

        public CommsService(IOptions<AppSettings> appSettings, IHandleComms<ICommsModel> sampleTestCommsHandler, ILogger<CommsService> logger)
        {
            _logger = logger;
            _commsSettings = appSettings.Value.CommsSettings;
            _sampleTestCommsHandler = sampleTestCommsHandler;
        }

        public async Task HandleSampleTestCommsAsync(SendTestCommsCommand command)
        {
            _logger.LogInformation($"Recieved SendTestCommsCommand for user with userId=[{command.UserId}]");
            command.Validate(_commsSettings.EmailValidationRegex, _commsSettings.UserIdUpperLimit, _logger);
            await _sampleTestCommsHandler.HandleAsync(new SampleTestComms() { UserId = command.UserId, Email = command.Email });
        }
    }
}