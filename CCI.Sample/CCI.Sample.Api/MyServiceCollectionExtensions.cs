﻿using CCI.Sample.Core;
using CCI.Sample.Core.Adapters.Http;
using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Handlers;
using CCI.Sample.Core.Handlers.Security;
using CCI.Sample.Core.Models;
using CCI.Sample.Core.Providers;
using CCI.Sample.Core.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCI.Sample.Api.Core
{
    public static class MyServiceCollectionExtensions
    {
        public static IServiceCollection AddMyCoreServices(this IServiceCollection services)
        {
            services.AddScoped<ITestService, TestService>();
            services.AddCommsServices();
            services.AddAuthenticationServices();

            return services;
        }

        public static IServiceCollection AddMyDALServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IEntityRepository<>), typeof(EntityRepository<>));
            services.AddScoped<IDatabaseContext, MyDatabaseContext>();

            return services;
        }

        public static IServiceCollection AddCommsServices(this IServiceCollection services)
        {
            services.AddScoped<IEmailCommsProvider, SendGridEmailProvider>();

            services.AddScoped<IHandleComms<ICommsModel>>(s => s.GetRequiredService<CommsHandler<ICommsModel>>());
            services.AddScoped<CommsHandler<ICommsModel>, SampleTestCommsHandler>();

            services.AddScoped<ICommsService, CommsService>();

            return services;
        }

        public static IServiceCollection AddAuthenticationServices(this IServiceCollection services)
        {
            services.AddTransient<IAuthenticationService, AuthenticationService>();

            return services;
        }

        public static IServiceCollection AddAdapters(this IServiceCollection services)
        {
            services.AddTransient(typeof(IQueryAdapter<,>), typeof(QueryAdapter<,>));

            return services;
        }

        public static IServiceCollection AddApiResolvers(this IServiceCollection services)
        {
            services.AddTransient<IApiUrlResolver>(x => { return new ApiRestfulUrlResolver(x.GetService<IOptions<AppSettings>>()); });
            return services;
        }
    }
}
