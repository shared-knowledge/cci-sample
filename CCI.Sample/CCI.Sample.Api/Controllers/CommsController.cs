﻿using CCI.Sample.Core.Commands;
using CCI.Sample.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CCI.Sample.Api.Controllers
{
    public class CommsController : Controller
    {
        readonly ILogger<CommsController> _logger;
        private readonly ICommsService _commsService;

        public CommsController(ICommsService commsService, ILogger<CommsController> logger)
        {
            _logger = logger;
            _commsService = commsService;
        }

        [Route("comms/sendTestComms")]
        [HttpPost]
        public async Task<IActionResult> SendSampleTestCommsAsync([FromBody]SendTestCommsCommand command)
        {
            await _commsService.HandleSampleTestCommsAsync(command);
            return Ok("You've got mail. Check your spam folder!");
        }
    }
}
