﻿using CCI.Sample.Core.Commands;
using CCI.Sample.Core.Queries;
using CCI.Sample.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CCI.Sample.Api.Controllers
{
    [Produces("application/json")]
    public class AuthController : Controller
    {
        readonly ILogger<AuthController> _logger;
        private readonly IAuthenticationService _authService;

        public AuthController(IAuthenticationService authService, ILogger<AuthController> logger)
        {
            _logger = logger;
            _authService = authService;
        }

        [Route("auth/register")]
        [HttpPost]
        public async Task<IActionResult> RegisterAsync([FromBody]CreateAuthenticationCommand command)
        {
            await _authService.CreateAuthenticationAsync(command);
            return Ok();
        }

        [Route("auth/login")]
        [HttpPost]
        public async Task<IActionResult> LoginAsync([FromBody]LoginCommand command)
        {
            return Ok($"AuthToken : {await _authService.LoginAsync(command)}");
        }

        [Route("auth/ping")]
        [HttpPost]
        public IActionResult AuthenticatedPing([FromBody]ValidateTokenQuery query)
        {
            return Ok($"AuthToken is Valid : {_authService.ValidateToken(query)}");
        }
    }
}