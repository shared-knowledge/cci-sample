﻿using CCI.Sample.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CCI.Sample.Api.Controllers
{
    public class HealthController : Controller
    {
        readonly ILogger<HealthController> _logger;
        private ITestService _testService;

        public HealthController(ITestService testService, ILogger<HealthController> logger)
        {
            _logger = logger;
            _testService = testService;
        }

        [Route("api/health")]
        [HttpGet]
        public IActionResult Health()
        {
            _logger.LogInformation("Successfully Executed api/health");
            return Ok("Api Healthy");
        }

        [Route("db/health")]
        [HttpGet]
        public async Task<IActionResult> DbCrudHealthAsync()
        {
            _logger.LogInformation("Executing db/health");

            await _testService.TestDbInsertAsync();

            _logger.LogInformation("Executed db/health");

            return Ok("Db Available");
        }
    }
}
