﻿using System.IO;
using CCI.Sample.Api.Core;
using CCI.Sample.Core;
using CCI.Sample.Core.AspNetExtensions;
using CCI.Sample.Core.DataAccess;
using CCI.Sample.Core.Handlers.Security;
using CCI.Sample.Core.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;

namespace CCI.Sample.Api 
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment hostEnv)
        {
            Configuration = new ConfigurationBuilder().WithConfiguration(hostEnv).Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<AuthenticationTokenHandler>();
            services.AddSingleton<PasswordHandler>();

            services.Configure<AppSettings>(Configuration.GetSection("appSettings"));
            services.AddTransient((s) => s.GetRequiredService<IOptions<AppSettings>>().Value);
            services.AddHttpClient();

            services.AddMyDALServices()
                    .AddAdapters()
                    .AddApiResolvers()
                    .AddMyCoreServices();

            services.AddDbContext<MyDatabaseContext>(optionsBuilder => optionsBuilder.UseMySql(Configuration.GetConnectionString("ConnectionString")));
            services.AddMvc();
            services.AddSwaggerDocumentationService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware(typeof(MetricsMiddleware));
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMvc();
            app.UseSwagger();

            loggerFactory.AddFile(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, @"Logs\log.txt"));

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CCI.Sample Api");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
