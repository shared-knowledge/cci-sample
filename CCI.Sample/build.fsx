#r "paket: groupref fake //"
#load "./.fake/build.fsx/intellisense.fsx"
#if !FAKE
  #r "netstandard"
#endif

open Fake
open System
open Fake.IO
open Fake.Core
open Fake.Tools
open Fake.DotNet
open Fake.Core.TargetOperators
open Fake.IO.Globbing.Operators
open Fake.IO.FileSystemOperators

// Properties
let doNothing           = ignore
let currentDirectory    = IO.Directory.GetCurrentDirectory ()

// dirs
let nupkgsDir           = "nupkgs"

// configs
let configuration      = Environment.environVarOrDefault "CONFIG" "Debug"
let buildConfig        = DotNet.BuildConfiguration.fromString configuration
let debug              = Environment.environVarAsBool "DEBUG"
let ansicolor          = Environment.environVarAsBool "ANSICOLOR"
let isCi               = Environment.environVarOrNone "CI" |> Option.isSome
let gitVersion         = GitVersion.generateProperties id
let imageTag           = if not isCi then "dev" else gitVersion.SemVer

if ansicolor then
    [ ConsoleTraceListener(
        CoreTracing.importantMessagesToStdErr,
        ConsoleWriter.colorMap,
        true) :> ITraceListener ]
    |> CoreTracing.setTraceListeners

module List = let consIf (arg: 'a) (cond: bool) (args: 'a list) : 'a list = if cond then arg :: args else args

// Clean Build Directory
Target.create "Clean" <| fun tp ->
    let shouldClean =  String.Compare(tp.Context.FinalTarget, "clean", true) = 0 || tp.Context.Arguments |> List.contains "--clean" 

    let clean () =
        let r = DotNet.exec id "clean" "CCI.Sample.sln /v:m"
        if r.ExitCode <> 0 then failwithf "dotnet clean failed to run successfully"

        !! "**/bin"
        ++ "**/artifacts"
        ++ "logs"
        ++ "test-results"
        ++ nupkgsDir
        |> Shell.cleanDirs

    match shouldClean with
    | true -> clean ()
    | false -> Trace.logfn "Skipping clean, no --clean argument specified"

// Restore
Target.create "Restore" <| fun _ ->
    DotNet.restore id "CCI.Sample.sln"

// Build All Projects in Build Directory
Target.create "Build" <| fun _ ->
    let args =
        [ "--no-restore"; @"/p:TreatWarningsAsErrors=""true"""; "/warnaserror" ]
        |> List.consIf "/consoleloggerparameters:ForceConsoleColor" ansicolor

    "CCI.Sample.sln"
    |> DotNet.build (fun p ->
        { p with Configuration = buildConfig }
        |> DotNet.Options.withAdditionalArgs args)

// Run all Unit Tests
Target.create "Test:Unit" <| fun _ ->
    let outputPath = currentDirectory </> "test-results/unit-tests.trx"

    "CCI.Sample.UnitTests"
    |> DotNet.test (fun p ->
        { p with
            Configuration = buildConfig
            NoBuild = true
            NoRestore = true
            Logger = Some (sprintf "trx;LogFileName=%s" outputPath) })

// Gather Artifacts
Target.create "Artifacts:Api" <| fun _ ->
    "CCI.Sample.Api"
    |> DotNet.publish (fun p ->
        { p with
            Configuration = buildConfig
            OutputPath = Some "artifacts" }
        |> DotNet.Options.withAdditionalArgs [ "--no-build"; "--no-restore" ])

Target.create "Artifacts:Tests" <| fun _ ->
    "CCI.Sample.UnitTests"
    |> DotNet.publish (fun p ->
        { p with
            Configuration = buildConfig
            OutputPath = Some "artifacts" }
        |> DotNet.Options.withAdditionalArgs [ "--no-build"; "--no-restore" ])

Target.create "Artifacts:Db" <| fun _ ->
    "CCI.Sample.DbMigrations"
    |> DotNet.publish (fun p ->
        { p with
            Configuration = buildConfig
            OutputPath = Some "artifacts" }
        |> DotNet.Options.withAdditionalArgs [ "--no-build"; "--no-restore" ])

Target.create "Artifacts" doNothing

// Helo world ;)
Target.create "Default" (fun _ ->
  Trace.trace "Hello World from FAKE"
)

Target.create "Run" doNothing
Target.create "Test" doNothing

// Targets
"Clean"
    ==> "Restore"
    ==> "Build"
    ==> "Artifacts"
    ==> "Run"

"Clean"
    ==> "Restore"
    ==> "Build"
    ==> "Test:Unit"
    ==> "Test"

"Build" ==> "Artifacts:Api" ==> "Artifacts"
"Build" ==> "Artifacts:Db" ==> "Artifacts"
"Build" ==> "Artifacts:Tests" ==> "Artifacts"

Target.runOrDefaultWithArguments "Run"