﻿using DbUp;
using DbUp.Engine;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace CCI.Sample.DbMigrations
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var rootPath = AppContext.BaseDirectory;
            var databaseName = "cci_sample_db";
            var appSettingsFile = "appsettings.json";

            var configuration = new ConfigurationBuilder()
                                    .SetBasePath(rootPath)
                                    .AddJsonFile(appSettingsFile)
                                    .Build();

            var connectionString = configuration.GetConnectionString("ConnectionString");

            EnsureDatabase(connectionString, 60);

            var upgrader = DeployChanges
                            .To
                            .MySqlDatabase(connectionString)
                            .WithTransactionPerScript()
                            .WithScriptsFromFileSystem($"{rootPath}/Migrations")
                            .WithVariable("DatabaseName", databaseName)
                            .LogToConsole()
                            .Build();

            if (!UpdateDb(upgrader))
                return 1;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();

            return 0;
        }

        private static void EnsureDatabase(string connectionString, int timeout = 0)
        {
            if (string.IsNullOrEmpty(connectionString) || connectionString.Trim() == string.Empty)
                throw new InvalidOperationException("The connection string cannot be empty");

            var connectionStringBuilder = new MySqlConnectionStringBuilder(connectionString);

            var initialCatalog = connectionStringBuilder.Database;
            if (string.IsNullOrEmpty(initialCatalog) || initialCatalog.Trim() == string.Empty)
                throw new InvalidOperationException("The connection string does not specify a database name.");

            connectionStringBuilder.Database = "sys";
            var sysConnectionStringBuilder = connectionStringBuilder;
            var sysConnectionString = sysConnectionStringBuilder.ConnectionString;

            Console.WriteLine("Opening db connection...");

            using (var connection = new MySqlConnection(sysConnectionString))
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine();
                    Console.WriteLine("Unable to open database connection to {0}: {1}", connection.ConnectionString, connection.Database, ex);
                    Console.ResetColor();
                    throw;
                }

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine();
                Console.WriteLine("Creating database: {0}", initialCatalog);
                Console.ResetColor();

                // create db
                var scriptPath = Path.Combine(AppContext.BaseDirectory, "Scripts", "Ensure Database.sql");
                var createDbSql = File.ReadAllText(scriptPath);
                createDbSql = createDbSql.Replace("$DatabaseName$", initialCatalog);

                using (var command = new MySqlCommand(createDbSql, connection) { CommandType = CommandType.Text })
                {
                    if (timeout >= 0) command.CommandTimeout = timeout;
                    command.ExecuteNonQuery();

                    Console.WriteLine("Ensured database {0} exists", initialCatalog);
                }
            }
        }

        private static bool UpdateDb(UpgradeEngine upgradeEngine)
        {
            var result = upgradeEngine.PerformUpgrade();
            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
                return false;
            }
            if (result.Scripts.Any())
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine();
                Console.WriteLine("The following scripts were executed:");
                Console.ResetColor();

                foreach (var script in result.Scripts)
                    Console.WriteLine(script.Name);

                Console.WriteLine();
            }

            return true;
        }
    }
}
